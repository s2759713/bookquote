package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    private HashMap<String, Double> isbnAndPrice = new HashMap<>();

    Quoter() {
        isbnAndPrice.put("1", 10.0);
        isbnAndPrice.put("2", 45.0);
        isbnAndPrice.put("3", 20.0);
        isbnAndPrice.put("4", 35.0);
        isbnAndPrice.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        if (isbnAndPrice.containsKey(isbn)) {
            return isbnAndPrice.get(isbn);
        }
        return 0.0;
    }

}
